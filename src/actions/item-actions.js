import {ACCESS_TOKEN} from "../constants";

const login = (apiService, dispatch) => (user) => {
    dispatch(postLoginRequested());
    apiService.login(user)
        .then((data) => {
            // localStorage.setItem(ACCESS_TOKEN, data.token);
            dispatch(postLoginLoaded(data))
        })
        .catch((err) => {
            alert(err)
            dispatch(postLoginError(err))
        });
}

const signup = (apiService, dispatch) => (user) => {
    dispatch(postRegisterRequested());
    apiService.signup(user)
        .then((data) =>  {
            dispatch(postRegisterLoaded(data))
        })
        .catch((err) => {
            alert(err)
            dispatch(postRegisterError(err))
        });
}

const payTip = (apiService, dispatch) => (tip) => {
    dispatch(payTipRequested());
    apiService.payTip(tip)
        .then((data) => {
            dispatch(payTipLoaded(data))
        })
        .catch((err) => {
            alert(err)
            dispatch(payTipError(err))
        });
}

const postLoginRequested = () => {
    return {
        type: 'POST_LOGIN_REQUEST'
    };
};
const postLoginLoaded = (data) => {
    console.log(data)
    return {
        type: 'POST_LOGIN_SUCCESS',
        payload: data
    };
};
const postLoginError= (error) => {
    console.log("ERROR: " + error)
    return {
        type: 'POST_LOGIN_FAILURE',
        payload: error
    };
};

const postRegisterRequested = () => {
    return {
        type: 'POST_REGISTER_REQUEST'
    };
};
const postRegisterLoaded = (data) => {
    console.log(data)
    return {
        type: 'POST_REGISTER_SUCCESS',
        payload: data
    };
};
const postRegisterError= (error) => {
    console.log("ERROR: " + error)
    return {
        type: 'POST_REGISTER_FAILURE',
        payload: error
    };
};

const payTipRequested = () => {
    return {
        type: 'POST_PAY_TIP_REQUEST'
    };
};
const payTipLoaded = (data) => {
    return {
        type: 'POST_PAY_TIP_SUCCESS',
        payload: data
    };
};
const payTipError= (error) => {
    return {
        type: 'POST_PAY_TIP_FAILURE',
        payload: error
    };
};

export {
    login,
    signup,
    payTip
}