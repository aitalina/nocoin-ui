import React from 'react';
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ACCESS_TOKEN } from "../constants";

class PrivateRoute extends React.Component {
	render() {
		const { component: Component } = this.props;
		return (
			<Route
				{...this.props.rest}
				render={props =>
					localStorage.getItem(ACCESS_TOKEN) !== null && localStorage.getItem(ACCESS_TOKEN) !== "null"? (
						<Component role={this.props.role} {...this.props.rest} {...props} />
					) : (
						<Redirect
							to={{
								pathname: '/login',
								state: { from: props.location }
							}}
						/>
					)
				}
			/>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		token: state.token,
		profileId: state.profileId
	}
};

export default connect(mapStateToProps)(PrivateRoute);
