import {ACCESS_TOKEN, API_BASE_URL} from "../constants";

export default class ApiService {

    clientSecret = async (user) => {
        const res = await fetch("http://localhost:4242/create-payment-intent", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ items: [{ id: "xl-tshirt" }] }),
        })
        if (!res.ok) {
            let body = await res.text();
            throw new Error(`Ошибка: ${body}`)
        }
        return await res.json();
    }
    login = async (user) => {
        let url = `/login`
        const res = await fetch(`${API_BASE_URL}${url}`, {
            method: 'PUT',
            origin: API_BASE_URL,
            headers: {
               // 'Authorization': 'Bearer_' + localStorage.getItem(ACCESS_TOKEN),
                'Content-Type': 'application/json;charset=utf-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, HEAD, POST, DELETE, OPTIONS',
            },
            body: JSON.stringify(user)
        })
         if (!res.ok) {
             let body = await res.text();
             throw new Error(`Ошибка: ${body}`)
         }
         return await res.json();
    }

    signup = async (user) => {
        let url = `/profile`
        console.log("ljh")
        const res = await fetch(`${API_BASE_URL}${url}`, {
            method: 'POST',
            origin: API_BASE_URL,
            headers: {
                //'Authorization': 'Bearer_' + localStorage.getItem(ACCESS_TOKEN),
                'Content-Type': 'application/json;charset=utf-8',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST, PUT, GET, OPTIONS',
                'Access-Control-Allow-Headers': '*'
            },
            body: JSON.stringify(user)
        })
        if (!res.ok) {
            let body = await res.text();
            throw new Error(`Ошибка: ${body}`)
       }
        return await res.json();
    }

    payTip = async (tip) => {
        let url = `/tip`
        const res = await fetch(`${API_BASE_URL}${url}`, {
            method: 'POST',
            origin: API_BASE_URL,
            headers: {
                // 'Authorization': 'Bearer_' + localStorage.getItem(ACCESS_TOKEN),
                'Content-Type': 'application/json;charset=utf-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST, PUT, GET, OPTIONS',
            },
            body: JSON.stringify(tip)
        })
        if (!res.ok) {
            let body = await res.text();
            throw new Error(`Ошибка: ${body}`)
        }
        return await res.json();
    }
}