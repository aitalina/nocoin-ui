import React, { Component } from 'react';
import {login, payTip} from "../../actions/item-actions";
import {withApiService} from "../hoc";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {Alert, Modal} from "react-bootstrap";
import { Route, Redirect } from "react-router-dom";


class MainContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			restaurantId: '',
			count: '',
			clientSecret: ''
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		let path = this.props.location.pathname;
		const restaurantId = path.substring(1, path.length);

		this.setState({
			...this.state,
			restaurantId: restaurantId
		});
	}

	handleInputChange(event) {
		const target = event.target;
		const inputName = target.name;
		let inputValue = target.value;
		if (inputName === "count" && inputValue.length !== 0) {
			inputValue = parseInt(inputValue);
			if (Number.isInteger(inputValue)) {
				this.setState({
					[inputName]: inputValue
				});
			}
		}
	}

	handleSubmit(event) {
		event.preventDefault();

		const payTipRequest = Object.assign({}, this.state);
		this.props.payTip(payTipRequest);
		this.setState({
			...this.state,
			count: ''
		});
	}

	render() {
		return (
			<>
				{this.props?.clientSecret && <Redirect to={'/pay'}/>}
				<main className='wrapper__main main'>
					<div className="main__container">
						<div className="main__study study-main">
							<div className="study-main__body">
								<div className="study-main__info-box">
									<div className='study-main__info'>
										<form onSubmit={this.handleSubmit} className='form-login'>
											<div className="form-item">
												<input type="count" name="count"
													   className="form-control" placeholder="sum"
													   value={this.state.count} onChange={this.handleInputChange}
													   required
													   autoComplete="off"/>
											</div>

											<div className="form-item">
												<button type='submit' className="button button_green" disabled={!this.state.count}>
													<div style={{color: 'white'}}>Pay</div>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
			</>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		clientSecret: state.clientSecret
	}
}

const mapDispatchToProps = (dispatch, ownProperty) => {
	const {apiService} = ownProperty;
	return {
		payTip: payTip(apiService, dispatch)
	}
}

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(MainContent)));