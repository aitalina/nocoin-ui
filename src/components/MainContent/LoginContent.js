import React, { Component } from 'react';
import {Redirect, withRouter} from "react-router-dom";
import {login} from "../../actions/item-actions";
import {connect} from "react-redux";
import {withApiService} from "../hoc";


class LoginContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			login: '',
			password: '',
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		const target = event.target;
		const inputName = target.name;
		const inputValue = target.value;

		this.setState({
			[inputName]: inputValue
		});
	}

	handleSubmit(event) {
		event.preventDefault();

		const loginRequest = Object.assign({}, this.state);
		this.props.login(loginRequest);
	}

	render() {
		if (this.props?.tipInfo !== null) {
			return <Redirect to={"/profile"}/>
		}
		return (
			<>
				<main className='wrapper__main main'>
					<div className="main__container">
						<div className="main__study study-main">
							<div className="study-main__body">
								<div className="study-main__info-box">
									<div className='study-main__info'>
										<form onSubmit={this.handleSubmit} className='form-login'>
											<div className="form-item">
												<input type="email" name="email"
													   className="form-control" placeholder="email"
													   value={this.state.email} onChange={this.handleInputChange}
													   required
													   autoComplete="off"/>
											</div>
											<div className="form-item">
												<input type="password" name="password"
													   className="form-control" placeholder="password"
													   value={this.state.password} onChange={this.handleInputChange}
													   required
													   autoComplete="off"/>
											</div>

											<div className="form-item">
												<button type='submit' className="button button_green">
													<div style={{color: 'white'}}>Login</div>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
			</>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		tipInfo: state.tipInfo
	}
}

const mapDispatchToProps = (dispatch, ownProperty) => {
	const {apiService} = ownProperty;
	return {
		login: login(apiService, dispatch)
	}
}

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginContent)));
