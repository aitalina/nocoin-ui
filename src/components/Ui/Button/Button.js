import React, { Component } from 'react';


class MainContent extends Component {
	constructor({ variant }) {
		super();

		this.className = variant === 'green' ? 'button' : 'button button_white';
	}

	render() {
		return (
			<button type='button' className={this.className}>
				<a href="/studypipes">Узнать больше</a>
				<svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M16 1.5V16.5M16 16.5H1M16 16.5L1 1.5" stroke="#000016" strokeWidth="2" />
				</svg>
			</button>
		)
	}
}

export default MainContent;