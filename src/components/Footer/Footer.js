import React, { Component } from 'react';


class Footer extends Component {
	constructor({ contacts }) {
		super();
		this.year = new Date().getFullYear();
	}

	render() {
		return (
			<>
				<footer className="wrapper__footer footer">
					<div className="footer__container">
						<div className="footer__body">
							<div className="footer__main main-footer">
								<div className="main-footer__body">
									<div className="main-footer__protect-info">© Все права защищены, 2010-{this.year} </div>
								</div>
							</div>
							<div className="footer__bottom bottom-footer">
								<div className="bottom-footer__body">
									<div className="bottom-footer__empty"></div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</>
		)
	}
}

export default Footer;