import React, { Component } from 'react';


class UsesResource extends Component {
	constructor({ resource }) {
		super();
		this.link = resource.link;
		this.title = resource.title;
	}

	render() {
		return (
			<div className='bottom-footer__sub-title'> <p>{`Image by <a href="${this.link}">${this.title}</a> `}</p></div>
		)
	}
}

export default UsesResource;