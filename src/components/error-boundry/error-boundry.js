import React from 'react';
export default class ErrorBoundry extends React.Component {

    state = {
        hasError: false
    }

    componentDidCatch(error, errorInfo) {
        this.setState(  { hasError: true });
    }

    render() {
        if (this.state.hasError) {
            return <div>Error!</div>;
        }

        return this.props.children;
    }

}