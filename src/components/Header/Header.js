import React, { Component } from 'react';

import {Link} from "react-router-dom";

class Header extends Component {
	constructor() {
		super();
		this.state = {
			classNameHeader: 'header__body',
			isOpenBurger: true
		}
	}

	render() {
		return (
			<>
				<header className="wrapper__header header">
					<div className="header__container">
						<div className={this.state.classNameHeader}>
							<h1 style={{color: 'white', fontSize: 40, fontWeight: 500}}>NOCOIN</h1>
							{/*<div>*/}
							{/*	<Link to={"/login"}>*/}
							{/*		<button type='button' style={{color: 'white', marginRight: '10px'}} className="button button_green">*/}
							{/*			Login*/}
							{/*	</button>*/}
							{/*	</Link>*/}
							{/*	<Link to={"/register"}>*/}
							{/*		<button type='button' style={{color: 'white'}} className="button button_green">*/}
							{/*			Create*/}
							{/*		</button>*/}
							{/*	</Link>*/}
							{/*</div>*/}
						</div>
					</div>
				</header>
			</>
		)
	}
}

export default Header;