import React, { Component } from 'react';
import './app.css';
import '../../scss/app.scss';
import { LoginPage, RegisterPage, UserPage, PaymentPage, SuccessPage } from "../pages";
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { withApiService } from "../hoc";
import { BreakpointProvider } from 'react-socks';
import Row from "react-bootstrap/Row";
import PrivateRoute from "../../common/PrivateRoute";
import MainPage from '../pages/MainPage';

class App extends Component {
	render() {
		return (
			<BreakpointProvider>
				<Row>
					<Switch>
						<Route path="/login" render={(props) => <LoginPage {...props} />} />
						<Route path="/register" component={RegisterPage} />
						<Route path="/profile" component={UserPage} />
						<Route path="/pay" component={PaymentPage} />
						<Route path="/success" component={SuccessPage} />
						<Route path="/*" render={(props) => <MainPage {...props} />} />
					</Switch>
				</Row>
			</BreakpointProvider>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		profileId: state.profileId,
		logout_flag: state.logout_flag
	}
};

const mapDispatchToProps = (dispatch, ownProperty) => {
	const { apiService } = ownProperty;
	return {

	}
}

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(App)));
