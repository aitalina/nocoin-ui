import LoginPage from "./login-page";
import UserPage from "./user-page";
import RegisterPage from "./signup-page";
import PaymentPage from "./payment-page"
import SuccessPage from "./success-page"

export {
    LoginPage,
    UserPage,
    RegisterPage,
    PaymentPage,
    SuccessPage
}