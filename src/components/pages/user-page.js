import React, {Component} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {withApiService} from "../hoc";
import {Table} from 'react-bootstrap';
import Header from "../Header/Header";
import Footer from "../Footer/Footer";

class UserPage extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {tipInfo} = this.props;

        if (this.props.tipInfo == null || this.props.tipInfo?.restaurantId == null) {
            return <Redirect to={"/login"}/>;
        }
        return (
            <>
                <Header/>
                <main className='wrapper__main main'>
                    <div className="main__container">
                        <div className="main__study study-main">
                            <div className="study-main__body">
                                <div className="study-main__info-box">
                                    <div className='study-main__info'>
                                        <Table style={{color: 'white', marginTop: '150px'}}>
                                            <thead>
                                            <tr>
                                                <th>Click to pay tip</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                                 <tr>
                                                    <td>
                                                        <Link to={tipInfo?.restaurantId} style={{color: 'white', marginTop: '150px'}}>
                                                            {tipInfo?.restaurantId}
                                                        </Link>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                        <Table style={{color: 'white', marginTop: '50px'}}>
                                            <thead>
                                            <tr>
                                                <th>Count</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {tipInfo?.tips?.map(t => {
                                                return <tr>
                                                    <td>{t?.count}</td>
                                                    <td>{t?.createDate}</td>
                                                </tr>
                                            })}
                                            </tbody>
                                        </Table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>

                <Footer/>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tipInfo: state.tipInfo
    }
};

const mapDispatchToProps = (dispatch, ownProperty) => {
    const {apiService} = ownProperty;
    return {}
}
export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(UserPage)));