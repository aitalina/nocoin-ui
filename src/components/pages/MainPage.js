import React, { Component } from 'react';

import Header from '../Header/Header';
import MainContent from '../MainContent/MainContent';
import Footer from '../Footer/Footer';

class MainPage extends Component {

	constructor() {
		super();
	}

	render() {
		return (
			<>
				<div className="wrapper">
					<Header />
					<MainContent />
					<Footer />
				</div>
			</>
		)
	}
}

export default MainPage;