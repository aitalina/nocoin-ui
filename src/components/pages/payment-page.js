import React, {Component} from 'react';
import './payment-page.css';
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import CheckoutForm from "../MainContent/CheckoutForm";
import {Redirect, withRouter} from "react-router-dom";
import {withApiService} from "../hoc";
import {connect} from "react-redux";


const stripePromise = loadStripe("pk_test_51Ok7UEBJtLYTpT2qe2r436T2YxjVZRINPsjjyIPRjvnaw83crgrSAquinin4IH9xdbg7ckYspt4iRCvik511tcpy00LwNEVdn3");

class PaymentPage extends Component {

    render() {
        return (
            <div className="App">
                {this.props?.clientSecret && (
                    <Elements options={{theme: 'stripe', clientSecret: this.props.clientSecret}} stripe={stripePromise}>
                        <CheckoutForm/>
                    </Elements>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        clientSecret: state.clientSecret
    }
}

const mapDispatchToProps = (dispatch, ownProperty) => {
    const {apiService} = ownProperty;
    return {

    }
}

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(PaymentPage)));
