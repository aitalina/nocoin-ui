import React, {Component} from 'react';
import './login-page.css'
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import LoginContent from "../MainContent/LoginContent";

class SuccessPage extends Component {

    render() {
        return (
            <>
                <div className="wrapper">
                    <Header/>
                    <main className='wrapper__main main'>
                        <div className="main__container">
                            <div className="main__study study-main">
                                <div className="study-main__body">
                                    <div className="study-main__info-box">
                                        <div className='study-main__info'>
                                            <h style={{color: 'white'}}>Success!</h>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                    <Footer/>
                </div>
            </>
        );
    }
}

export default SuccessPage;
