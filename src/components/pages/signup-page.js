import React, {Component} from 'react';

import './signup-page.css'
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import {signup} from "../../actions/item-actions";
import {withApiService} from "../hoc";
import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router-dom";


class SignupPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            restaurantName: '',
            password: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;

        this.setState({
            [inputName]: inputValue
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const loginRequest = Object.assign({}, this.state);
        this.props.signup(loginRequest);
    }
    render() {
        if (this.props?.restaurant !== null && this.props?.restaurant?.id?.length > 0) {
            return <Redirect to={"/login"}/>;
        }
        return (
            <>
                <div className="wrapper">
                    <Header/>
                    <main className='wrapper__main main'>
                        <div className="main__container">
                            <div className="main__study study-main">

                                <div className="study-main__body">

                                    <div className="study-main__info-box">

                                        <div className='study-main__info'>
                                            <form onSubmit={this.handleSubmit} className='form-login'>
                                                <div className="form-item">
                                                    <input type="restaurantName" name="restaurantName"
                                                           className="form-control" placeholder="restaurant name"
                                                           value={this.state.restaurantName}
                                                           onChange={this.handleInputChange}
                                                           required
                                                           autoComplete="off"/>
                                                </div>
                                                <div className="form-item">
                                                    <input type="name" name="name"
                                                           className="form-control" placeholder="name"
                                                           value={this.state.name} onChange={this.handleInputChange}
                                                           required
                                                           autoComplete="off"/>
                                                </div>
                                                <div className="form-item">
                                                    <input type="email" name="email"
                                                           className="form-control" placeholder="email"
                                                           value={this.state.email} onChange={this.handleInputChange}
                                                           required
                                                           autoComplete="off"/>
                                                </div>
                                                <div className="form-item">
                                                    <input type="password" name="password"
                                                           className="form-control" placeholder="password"
                                                           value={this.state.password} onChange={this.handleInputChange}
                                                           required
                                                           autoComplete="off"/>
                                                </div>

                                                <div className="form-item">
                                                    <button type='submit' className="button button_green">
                                                        <div style={{color: 'white'}}>Create</div>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                    <Footer/>
                </div>
            </>
        )

    }
}

const mapStateToProps = (state) => {
    return {
        restaurant: state.restaurant
    }
}

const mapDispatchToProps = (dispatch, ownProperty) => {
    const {apiService} = ownProperty;
    return {
        signup: signup(apiService, dispatch)
    }
}

export default withApiService()(connect(mapStateToProps, mapDispatchToProps)(withRouter(SignupPage)));
