import React, {Component} from 'react';
import './login-page.css'
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import LoginContent from "../MainContent/LoginContent";

class LoginPage extends Component {

    render() {
        return (
            <>
                <div className="wrapper">
                    <Header />
                    <LoginContent />
                    <Footer />
                </div>
            </>
        );
    }
}
export default LoginPage;
