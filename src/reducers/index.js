
const initialState = {
    restaurant: null,
    tip: null,
    tipInfo: null,
    clientSecret: null
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'POST_REGISTER_REQUEST' :
            return {
                ...state
            };
        case 'POST_REGISTER_SUCCESS':
            return {
                ...state,
                restaurant: action.payload
            };
        case 'POST_REGISTER_FAILURE' :
            return {
                ...state
            };
        case 'POST_LOGIN_REQUEST' :
            return  {
                ...state
            };
        case 'POST_LOGIN_SUCCESS':
            return  {
                ...state,
                tipInfo: action.payload
            };
        case 'POST_LOGIN_FAILURE':
            return {
                ...state
            }
        case 'POST_PAY_TIP_REQUEST':
            return {
                ...state
            }
        case 'POST_PAY_TIP_SUCCESS':
            return {
                ...state,
                clientSecret: action.payload.clientSecret
            }
        case 'POST_PAY_TIP_FAILURE':
            return {
                ...state,
                clientSecret: null
            }
        default:
            return state;

    }
};

export default reducer;